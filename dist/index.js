'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = _interopDefault(require('react'));
var styled = require('styled-components');
var styled__default = _interopDefault(styled);

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _taggedTemplateLiteral(strings, raw) {
  if (!raw) {
    raw = strings.slice(0);
  }

  return Object.freeze(Object.defineProperties(strings, {
    raw: {
      value: Object.freeze(raw)
    }
  }));
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n      font-weight: ", ";\n      background-color: ", ";\n      color: ", ";\n      width: ", ";\n      height: ", ";\n      font-size: ", ";\n    "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin: 2vh 0;\n  border-radius: 0.8vw;\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}
var size = {
  small: _defineProperty({
    width: "10rem",
    height: "2rem"
  }, "font-size", "1rem"),
  medium: _defineProperty({
    width: "15rem",
    height: "2.5rem"
  }, "font-size", "1rem"),
  large: _defineProperty({
    width: "20rem",
    height: "3rem"
  }, "font-size", "1rem"),
  getSize: function getSize(size) {
    return this[size] || this["medium"];
  }
};
var Button = styled__default.button(_templateObject(), function (props) {
  return styled.css(_templateObject2(), props.weight || 500, props.background || "#222", props.color || "#222", size.getSize(props.size)["width"], size.getSize(props.size)["height"], size.getSize(props.size)["font-size"]);
});

/* Core */

var Button$1 = function Button$1(props) {
  return React.createElement(Button, props, props.children);
};

exports.Button = Button$1;
