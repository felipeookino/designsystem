import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';

export default {
  input: 'src/index.js',
  output: {
    
    dir: 'dist',
    name: 'kim-styled',
    format: 'esm',
    inlineDynamicImports: true,
  },
  external: [
    'react',
    'react-dom'
  ],
  plugins: [
    babel({
      exclude: 'node_modules/**'
    }),
    commonjs({
      include: 'node_modules/**'
    })
  ],

};