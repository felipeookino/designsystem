import React, { useState } from "react";
import Button from "../.././../src/components/Button/button";
import { action } from "@storybook/addon-actions";

import { FaReact } from "react-icons/fa";
export default {
  title: "Form",
  component: Button
};

export const ToStorybook = () => {
  const [active, setActive] = useState(null);
  return (
    <div
      style={{
        display: "grid",
        gridTemplateColumns: "repeat(2, 1fr)",
        width: "100%",
        alignItems: "center"
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "100%",
          alignItems: "center"
        }}
      >
        <h1>TextButton</h1>
        <label>Rounded + Outlined </label>

        <Button
          onClick={() =>
            setActive({
              weight: "300",
              size: "medium",
              color: "#FF2F2F",
              shape: "rounded",
              variant: "outlined"
            })
          }
          weight="300"
          size="medium"
          color="#FF2F2F"
          shape="rounded"
          variant="outlined"
        >
          <FaReact />
          Click Me
        </Button>
        <label>Rounded + Contained </label>
        <Button
          onClick={() =>
            setActive({
              weight: "300",
              size: "medium",
              background: "#FF2F2F",
              shape: "rounded",
              variant: "contained"
            })
          }
          weight="300"
          size="medium"
          background="#FF2F2F"
          color="#fff"
          shape="rounded"
          variant="contained"
        >
          <FaReact />
          Click Me
        </Button>
        <label>Rounded + Pattern </label>
        <Button
          onClick={() =>
            setActive({
              weight: "300",
              size: "medium",
              color: "#FF2F2F",
              shape: "rounded",
              variant: "pattern"
            })
          }
          weight="300"
          size="medium"
          color="#FF2F2F"
          shape="rounded"
          variant="pattern"
        >
          <FaReact />
          Click Me
        </Button>
        <label>Square + Outlined </label>
        <Button
          onClick={() =>
            setActive({
              weight: "300",
              size: "medium",
              color: "FF2F2F",
              shape: "square",
              variant: "outlined"
            })
          }
          weight="300"
          color="#FF2F2F"
          size="medium"
          shape="square"
          variant="outlined"
        >
          <FaReact />
          Click Me
        </Button>
        <label>Square + Contained </label>
        <Button
          onClick={() =>
            setActive({
              weight: "300",
              size: "medium",
              background: "#FF2F2F",
              shape: "square",
              variant: "contained"
            })
          }
          weight="300"
          size="medium"
          background="#FF2F2F"
          color="#fff"
          shape="square"
          variant="contained"
        >
          <FaReact />
          Click Me
        </Button>
        <label>Square + Pattern </label>
        <Button
          onClick={() =>
            setActive({
              weight: "300",
              size: "medium",
              shape: "square",
              color: "#FF2F2F",
              variant: "pattern"
            })
          }
          weight="300"
          size="medium"
          color="#FF2F2F"
          shape="square"
          variant="pattern"
        >
          <FaReact />
          Click Me
        </Button>
      </div>
      {active && (
        <div>
          <h3>Button Properties</h3>
          <ul style={{ listStyle: "none" }}>
            {`import { Button } from 'designsystem';`}
            <br />
            {`<Button onClick={() => {}} `}
            {Object.keys(active).map(prop => (
              <li>&nbsp;&nbsp;&nbsp;{`${prop}="${active[prop]}"`}</li>
            ))}
            {`>Click Me</Button>`}
          </ul>
        </div>
      )}
    </div>
  );
};

ToStorybook.story = {
  name: "TextButton"
};
