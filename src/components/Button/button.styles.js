import styled, { css } from "styled-components";

const size = {
  small: { width: "25%", ["font-size"]: "2.5vh" },
  medium: { width: "50%", ["font-size"]: "2.5vh" },
  large: { width: "75%", ["font-size"]: "2.5vh" },
  getSize: function(size) {
    return this[size] || this["medium"];
  }
};

const shape = {
  square: "0",
  rounded: "0.8vw",
  getShape: function(shape) {
    return this[shape] || this["square"];
  }
};

const variant = {
  outlined: "background-color: transparent; border: 1.2px solid",
  contained: function(color) {
    return `background-color: ${color}`;
  },
  pattern: "background-color: transparent; border: none",
  getVariant: function(variant, color) {
    if (variant === "contained") return this[variant](color);
    else return this[variant] || this["pattern"];
  }
};

export const Button = styled.button`
  margin: 2vh 0;
  display: flex;
  justify-content: space-around;
  outline: none;
  box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2),
    0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12);
  border: none;
  transition: 300ms;
  align-items: center;
  padding: 8px;
  ${props =>
    css`
      ${variant.getVariant(props.variant, props.background)};
      font-weight: ${props.weight || 500};
      color: ${props.color || "#222"};
      width: ${size.getSize(props.size)["width"]};
      height: ${size.getSize(props.size)["height"]};
      font-size: ${props.fontSize || size.getSize(props.size)["font-size"]};
      border-radius: ${shape.getShape(props.shape)};
    `}
  &:hover {
    box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.25),
      0px 2px 2px 0px rgba(0, 0, 0, 0.27), 0px 1px 5px 0px rgba(0, 0, 0, 0.27);
  }
  &:active {
    box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.05),
      0px 2px 2px 0px rgba(0, 0, 0, 0.07), 0px 1px 5px 0px rgba(0, 0, 0, 0.07);
  }
`;
